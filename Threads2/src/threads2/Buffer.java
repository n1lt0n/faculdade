/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package threads2;

import java.util.Stack;

/**
 *
 * @author nilton
 */
class Buffer {

    private Stack<Integer> sharedLocation = new Stack<Integer>();
    private int size;
    
    public Buffer(int i) {
        size=i;
    }
    public int get(){
        return sharedLocation.pop();
    }
    
    public void set(int v){
        sharedLocation.push(v);
    }
    
}
