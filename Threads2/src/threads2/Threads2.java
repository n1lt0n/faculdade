/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package threads2;

/**
 *
 * @author nilton
 */
public class Threads2 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Buffer buff = new Buffer(5000);
        
        Produtor producer = new Produtor(buff);
        Consumidor consumer1 = new Consumidor(buff);
        Consumidor consumer2 = new Consumidor(buff);
        
        try {
            producer.start();
            consumer1.start();
            consumer2.start();
        } catch (Exception e) {
        }
        
    }
}
