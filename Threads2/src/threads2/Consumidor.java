/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package threads2;

/**
 *
 * @author nilton
 */
class Consumidor extends Thread {

    private Buffer shared;
    public Consumidor(Buffer buff) {
        shared=buff;
    }

    @Override
    public void run() {
        System.out.println("Consumidor le: "+shared.get());
    }
    
}
