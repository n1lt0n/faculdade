package poo1;
/* Primeiro exemplo em Java - Prof. ALDO*/

public class Main {

    public static void main(String[] args) {
        System.out.println("** Sistema Bancario **");

        Conta c1 = new Conta("21.342-7");
        c1.creditar(875.32);
        c1.imprimirSaldo();
        c1.creditar(20);
        c1.imprimirSaldo();
    }

}
