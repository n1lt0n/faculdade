package poo1;

public class Conta {
    private String numero;
    private double saldo;

    public Conta(String numero) {
        this.numero = numero;
        saldo = 0;
    }

    public void creditar(double valor) {
        saldo = saldo + valor;
    }

    public void imprimirSaldo() {
        System.out.println("Conta: " +
                numero + " Saldo: R$" + saldo);
    }

    public void transferir(Conta destino, double valor) {
        //TODO: desenvolver esta função
    }
}