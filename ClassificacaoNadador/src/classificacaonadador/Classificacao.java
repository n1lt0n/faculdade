/*
 Infantil A = 5 a 7 anos
 Infantil B = 8 a 10 anos
 Juvenil A = 11 a 13 anos
 Juvenil B = 14 a 17 anos
 Adulto = Acima de 18 Anos
 */

package classificacaonadador;

/**
 *
 * @author n1lt0n
 */
public class Classificacao {
    private int idade;

    public Classificacao(int i) {
        this.idade=i;
    }
    public String Resultado() {
        if (this.idade>4 && this.idade<8) {
            return "Infantil A";
        } else {
            if (this.idade>7 && this.idade<11){
                return "Infantil B";
            } else {
                if (this.idade>10 && this.idade<14){
                    return "Juvenil A";
                } else {
                    if(this.idade>13 && this.idade<18) {
                        return "Juvenil B";
                    } else {
                        if(this.idade>17){
                            return "Adulto";
                        } else {
                            return "Não Pode Competir";
                        }
                    }
                }
            }
        }
    }
}
