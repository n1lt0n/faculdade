/*
 * 1. Implemente as classes abaixo:
 * a) Uma classe Transporte com atributos ligado (tipo boolean) e velocidade (tipo int) e métodos
 * liga() e desliga(). O método liga torna o atributo ligado true e o método desliga torna o atributo
 * ligado false, além de tornar a velocidade zero. Crie também métodos get/set para modificar o
 * atributo velocidade, sendo que a velocidade não pode ser negativa.
*/
package transport;


public class Transporte {
    //atributos
    private boolean ligado;
    private int velocidade;
    
    //métodos
    public int getVelocidade(){
        return velocidade;
    }
    public void setVelocidade(int vel){
        this.velocidade=vel;
    }
    public void ligar(){
        ligado=true;
    }
    public void desligar(){
        ligado=false;
        velocidade=0;
    }
}