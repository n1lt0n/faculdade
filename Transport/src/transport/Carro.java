/*
 * b) Uma subclasse de Transporte chamada Carro. Carro deve ter o atributo quilometragem (tipo
 * int) e os métodos necessários para lê-lo e alterá-lo (get/set). A quilometragem não pode ser
 * negativa, nem ultrapassar o valor 999999. A velocidade do Carro não pode ser negativa, nem
 * ultrapassar 200.
 */
package transport;
public class Carro extends Transporte {
    //atributos
    private int quilometragem;
    //métodos
    public void setQuilometragem(int km){
        quilometragem=km;
    }
    public int getQuilometragem(){
        return quilometragem;
    }
    private boolean verificaKM(){
        if(quilometragem<0 || quilometragem>=999999){
            return false;
        }else {
            return true;
        }
    }
    private boolean verificaVel(){
        if(getVelocidade()<0 || getVelocidade()>=200) {
            return false;
        }else {
            return true;
        }
    }
}
