/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package mediaaritmeticaii;
public class Media {
    //atributos
    private float nota1;
    private float nota2;
    private float nota3;
    //métodos
    public Media(float n1,float n2,float n3){
        this.nota1 = n1;
        this.nota2 = n2;
        this.nota3 = n3;
    }
    public float calcularMedia(){
        return (nota1+nota2+nota3)/3;
    }
    public String obterMensagem(){
        float result = calcularMedia();
        if(result>5) {
            return "Média: "+result+"\nResultado: Aprovado";
        } else {
            return "Média: "+result+"\nResultado: Reprovado";
        }
    }

}
