/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fetch;

import javax.el.ELResolver;
import javax.faces.context.FacesContext;

/**
 *
 * @author nilton
 */
public class Place {

    private String[] mapUrls;
    private String weather;

    public String fetch() {
        FacesContext fc = FacesContext.getCurrentInstance();
        ELResolver elResolver = fc.getApplication().getELResolver(); // Get maps

        MapService ms = elResolver.getValue(fc.getELContext(), null, "mapService");

        mapUrls = ms.getMap(streetAddress, city, state); // Get weather

        WeatherService ws = elResolver.getValue(fc.getELContext(), null, "weatherService");

        weather = ws.getWeatherForZip(zip, true); // Get places

        Places places =  elResolver.getValue(fc.getELContext(), null, "places"); // Add new place to places

        places.addPlace(streetAddress, city, state, mapUrls, weather);

        return null;
    }
}
