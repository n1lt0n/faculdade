package exercicio3;

/**
 *
 * @author Nilton Tadeu Ferreira Filho
 */
public class DiasDoMes {
    private int ano;
    private int mes;

    public DiasDoMes(int a, int m) {
        this.ano = a;
        this.mes = m;
    }
    public int QuantosDias(){
        switch(this.mes){
            case 1:
                return 31;
            case 2:
                if((this.ano%400)==0){
                    return 29;
                }else {
                    return 28;
                }
            case 3:
                return 31;
            case 4:
                return 30;
            case 5:
                return 31;
            case 6:
                return 30;
            case 7:
                return 31;
            case 8:
                return 31;
            case 9:
                return 30;
            case 10:
                return 31;
            case 11:
                return 30;
            case 12:
                return 31;
            default:
                return 0;
        }
    }
}
