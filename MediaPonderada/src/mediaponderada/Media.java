/*
 * Pesos 2,3,5
 */
package mediaponderada;

/**
 *
 * @author n1lt0n
 */
public class Media {

    private int codigo;
    private double nota1;
    private double nota2;
    private double nota3;

    public Media(int c, double n1, double n2, double n3) {
        this.codigo = c;
        this.nota1 = n1;
        this.nota2 = n2;
        this.nota3 = n3;
    }

    public String resultado() {
        double res = ((this.nota1 * 2) + (this.nota2 * 3) + (this.nota3 * 5))/10;

        if (res >= 7) {
            return "Codigo: "+this.codigo+"\nMédia: " + res + "\nResultado: Aprovado";
        } else {
            return "Codigo: "+this.codigo+"\nMédia: " + res + "\nResultado: Reprovado";
        }
    }
}