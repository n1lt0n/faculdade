/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package aumentosalario;

/**
 *
 * @author fcg.1155376925
 */
public class Aumento {
    private int codigo;
    private String cargo;
    private int percentual;
    private double salario;

    public Aumento(int c, String ca, int per, double sal){
        this.codigo = c;
        this.cargo = ca;
        this.percentual = per;
        this.salario = sal;
    }

    public String resultado(){
        return "Salário Antigo: "+this.salario+"\nAumento: "+this.percentual+"%\nNovo Salário: "+((this.salario)+(this.salario*this.percentual/100));
    }
}
