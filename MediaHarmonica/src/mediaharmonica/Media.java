/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package mediaharmonica;

/**
 *
 * @author fcg.1155376925
 */
public class Media {
    private double nota1;
    private double nota2;
    private double nota3;
    private double nota4;

    public Media(double n1, double n2, double n3, double n4){
        this.nota1=n1;
        this.nota2=n2;
        this.nota3=n3;
        this.nota4=n4;
    }

    public String resultado(){
        return "Média: "+4/((1/this.nota1)+(1/this.nota2)+(1/this.nota3)+(1/this.nota4));
    }

}
