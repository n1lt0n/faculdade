/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package cangibrina;

/**
 *
 * @author n1lt0n
 */
public class Cardapio {
    private String especificacao;
    private int codigo;
    private double precoU;
    private int quantidade;
    private double precoT;

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getEspecificacao() {
        return especificacao;
    }

    public void setEspecificacao(String especificacao) {
        this.especificacao = especificacao;
    }

    public double getPrecoT() {
        return precoT;
    }

    public void setPrecoT(double precoT) {
        this.precoT = precoT;
    }

    public double getPrecoU() {
        return precoU;
    }

    public void setPrecoU(double precoU) {
        this.precoU = precoU;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }
    
    public String resultado(){
        return "Codigo: "+this.codigo+"\nItem: "+this.especificacao+
                "\nValor Unitário: "+this.precoU+"\nQuantidade: "+this.quantidade+
                "\nValor Total: "+this.precoT;
    }
}
