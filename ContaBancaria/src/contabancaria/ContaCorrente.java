package contabancaria;
/**
 *
 * @author fcg.1155376925
 */
public class ContaCorrente {
    private int numeroConta;
    private double valorSaldo;
    private double valorLimite;

    public void AbrirConta(int n, double s){
        this.numeroConta = n;
        this.valorSaldo = s;
    }

    public void DefinirLimite(double l){
        this.valorLimite = l;
    }

    public void Depositar(double v){
        this.valorSaldo = this.valorSaldo+v;
    }

    public double Debitar(double v) {
        this.valorSaldo = this.valorSaldo-v;
        return v;
    }

    public String ObterDados() {
        return "Numero da Conta: "+this.numeroConta+"\nValor do Saldo: "+this.valorSaldo+"\nLimite: "+this.valorLimite;
    }
}
