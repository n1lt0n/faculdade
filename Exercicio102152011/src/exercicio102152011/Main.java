/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package exercicio102152011;

import java.awt.event.*;
import java.awt.*;
import javax.swing.*;
import javax.swing.JMenuItem;

/**
 *
 * @author Nilton Tadeu Ferreira Filho
 */
public class Main extends JFrame {

    JTextField jTSalario;
    JButton jBCalcular;

    public Main() {
        //seta o titulo do container da classe
        setTitle("Rendimento");
        //seta o tamanho do container da classe
        setSize(320, 240);
        //adiciona um listener para a janela
        addWindowListener(new WindowAdapter() {
            //trata o evento de fechamento da janela

            @Override //sobrescreve o evento de fechamento de janela padrão da linguagem
            public void windowClosing(WindowEvent e) {
                //função que executa o fechamento da janela
                actionExit();
            }
        });
        //configura o menu arquivo
        JMenuBar menuBar = new JMenuBar();
        JMenu fileMenu = new JMenu("Arquivo");
        fileMenu.setMnemonic(KeyEvent.VK_F);
        JMenuItem fileExitMenuItem = new JMenuItem("Sair", KeyEvent.VK_X);
        fileExitMenuItem.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                actionExit();
            }
        });
        fileMenu.add(fileExitMenuItem);
        menuBar.add(fileMenu);
        setJMenuBar(menuBar);

        //configura panel de calculo
        JPanel rendPanel = new JPanel();
        GridBagConstraints constraints;
        GridBagLayout layout = new GridBagLayout();
        rendPanel.setLayout(layout);

        //configura a label salario no panel de rendimentos
        JLabel jLSalario = new JLabel("Salário:");
        constraints = new GridBagConstraints();
        constraints.anchor = GridBagConstraints.EAST;
        constraints.insets = new Insets(5, 5, 0, 0);
        layout.setConstraints(jLSalario, constraints);
        rendPanel.add(jLSalario);

        jTSalario = new JTextField();
        constraints = new GridBagConstraints();
        constraints.fill = GridBagConstraints.HORIZONTAL;
        constraints.gridwidth = GridBagConstraints.REMAINDER;
        constraints.insets = new Insets(5, 5, 0, 5);
        layout.setConstraints(jTSalario, constraints);
        rendPanel.add(jTSalario);

        jBCalcular = new JButton("Rendimentos");
        jBCalcular.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                actionRend();
            }
        });
        constraints = new GridBagConstraints();
        constraints.gridwidth = GridBagConstraints.REMAINDER;
        constraints.insets = new Insets(5, 5, 5, 5);
        layout.setConstraints(jBCalcular, constraints);
        rendPanel.add(jBCalcular);

        getContentPane().setLayout(new BorderLayout());
        getContentPane().add(rendPanel,BorderLayout.NORTH);
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Main prin = new Main();
        prin.setVisible(true);
    }

    private void actionExit() {
        System.exit(0);
    }

    private void actionRend() {
        Double rend1,rend2, saldo;
        rend1 = Double.parseDouble(jTSalario.getText());
        rend1 = (rend1*1.30)/100;
        rend2 = Double.parseDouble(jTSalario.getText());
        rend2 = (rend2*1.14)/100;
        saldo = rend1+rend2+(Double.parseDouble(jTSalario.getText()));
        JOptionPane.showMessageDialog(Main.this, "Rendimento mes 1: " + rend1 + " Rendimento mes 2: " + rend2 + " Saldo: " + saldo, "Rendimentos", JOptionPane.OK_OPTION);
    }
}
