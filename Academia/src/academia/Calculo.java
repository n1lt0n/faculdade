/*
 * Autor: Nilton Tadeu Ferreira Filho
 * RA: 1155376925
 */
package academia;

import java.text.DecimalFormat;

public class Calculo {
    //definição de atributos

    private String nome;
    private int idade;
    private double peso;
    private double altura;

    //definição de métodos
    //construtor da classe calculo
    public Calculo(String n, int i, double p, double a) {
        nome = n;
        idade = i;
        peso = p;
        altura = a;
    }
    //método para o calculo do IMC

    public double imc() {
        double aux = peso / (altura * altura);
        return aux;
    }
    //método para classificação pela idade
    public char classificacao() {
        if (peso <= 55) {
            if (idade <= 25) {
                return 'A';
            } else {
                if (idade >= 26 && idade <= 49) {
                    return 'D';
                } else {
                    return 'G';
                }
            }
        } else {
            if (peso >= 56 && peso <= 89) {
                if (idade <= 25) {
                    return 'B';
                } else {
                    if (idade >= 26 && idade <= 49) {
                        return 'E';
                    } else {
                        return 'H';
                    }
                }
            } else {
                if (idade <= 25) {
                    return 'C';
                } else {
                    if (idade >= 26 && idade <= 49) {
                        return 'F';
                    } else {
                        return 'I';
                    }
                }
            }
        }
    }
    //metodo para retornar a primeira mensagem
    public String mensagem1(){
        return "Nome: "+nome+"\nIMC: "+imc();
    }
    //método para retornar a segunda mensagem
    public String mensagem2(){
        return "Nome: "+nome+"\nIdade: "+idade+"\nClassificação: "+classificacao();
    }
}
