package contabancaria2;
public abstract class ContaBancaria {
    private String nome;
    private String cpf;
    private String numeroConta;
    private double saldo;

    public void create(String nome,String cpf){
        this.nome=nome;
        this.cpf=cpf;
        this.saldo=0.0;
    }
    public void create(String nome,String cpf, double saldo){
        this.nome=nome;
        this.cpf=cpf;
        this.saldo=saldo;
    }
    public void depositar(double quantia){
        this.saldo = this.saldo+quantia;
    }
    public double  sacar(double quantia){
        this.saldo=this.saldo-quantia;
        if(this.saldo<0){
            this.saldo=this.saldo+quantia;
            return -1.1;
        } else {
            return this.saldo;
        }
    }
    public double imprimirSaldo(){
        return this.saldo;
    }

}
