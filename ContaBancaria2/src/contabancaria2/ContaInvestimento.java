/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package contabancaria2;

import javax.swing.JOptionPane;

/**
 *
 * @author fcg.1155376925
 */
public class ContaInvestimento extends ContaBancaria{
    private double taxaDeAdministracao;
    private double saldoMinimo;

    public void create(String nome,String cpf,double taxaAdm, double saldoMin, double saldo) {
        this.taxaDeAdministracao=taxaAdm;
        this.saldoMinimo=saldoMin;
        if(saldo<saldoMin){
            JOptionPane.showMessageDialog(null, "Valor do saldo deve ser maior do que: "+saldoMinimo, "Erro", JOptionPane.ERROR_MESSAGE);
        }else {
            super.create(nome, cpf, saldo);
        }
    }
}
