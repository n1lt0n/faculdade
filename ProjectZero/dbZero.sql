//criacao da tabela Tresp
create sequence SqTresp start 1;
create table Tresp (
IdTresp integer primary key default nextval('SqTresp'),
Nome varchar(40) not null,
login varchar(100) not null,
senha varchar(100) not null,
cargo integer not null references Cargo(IdCargo))

//Criacao da Tabela Cargo
create sequence SqCargo start 1;
create table Cargo (
IdCargo integer primary key default nextval('SqCargo'),
Descricao text not null);

//
