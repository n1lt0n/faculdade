package ucfase1;
import javax.swing.JOptionPane;
public class UCFase1 {
    public static void main(String[] args) {
        UnidadeConsumidora uc = new UnidadeConsumidora();
        int valor=0;
        int i;
        int cont=1;
        uc.setCodigoUc(Integer.parseInt(JOptionPane.showInputDialog("Entre com o Código da Unidade Consumidora:")));
        uc.setNomeProprietario(JOptionPane.showInputDialog("Entre com o nume do proprietário"));
        uc.setEnderecoUc(JOptionPane.showInputDialog("Entre com o endereço da Unidade Consumidora:"));
        uc.setValorTarifa(0.34);
        String resultado="Código da Unidade Consumidora:"+uc.getCodigoUc()+"\nNome do Proprietário: "+uc.getNomeProprietario()+"\nEndereço da Unidade consumidora: "+uc.getEnderecoUc()+"\nValor da Tarifa: "+uc.getValorTarifa();
        do {
            for (i = 0; i < 2; i++) {
                valor = Integer.parseInt(JOptionPane.showInputDialog("Entre com a Leitura (valor negativo encerrará o programa):"));
                if (valor < 0) {
                    uc.setLeituraAnterior(-1);
                    uc.setLeituraAtual(-1);
                    break;
                } else {
                    if (i == 0) {
                        uc.setLeituraAnterior(valor);
                    } else {
                        uc.setLeituraAtual(valor);
                    }
                }
            }
            if(uc.getLeituraAtual()>=0 && uc.getLeituraAnterior()>=0){
                resultado=resultado+"\nValor a Pagar mes "+cont+": "+uc.ValorAPagar();
            }
            cont++;
        }while(valor>=0);
        JOptionPane.showMessageDialog(null, resultado, "Valores a Pagar", JOptionPane.INFORMATION_MESSAGE);
    }
}
