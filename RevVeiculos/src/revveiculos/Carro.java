package revveiculos;

public class Carro extends Veiculo {
    private int numPassageiros;
    private int acessorios;

    public Carro(String marca, String modelo, TipoVeiculo tipo, double preco, char combustivel, int ano, int numPassageiros, int acessorios) {
        super(marca, modelo, tipo, preco, combustivel, ano);
        this.numPassageiros = numPassageiros;
        this.acessorios = acessorios;
    }
    
    public int getAcessorios() {
        return acessorios;
    }

    public void setAcessorios(int acessorios) {
        this.acessorios = acessorios;
    }

    public int getNumPassageiros() {
        return numPassageiros;
    }

    public void setNumPassageiros(int numPassageiros) {
        this.numPassageiros = numPassageiros;
    }
    @Override
    public double obterPreco(){
        double aux=super.getPreco();
        if (super.getCombustivel()=='A') {
            aux=aux-(aux*0.08);
            return aux;
        } else {
            return aux;
        }
    }

}
