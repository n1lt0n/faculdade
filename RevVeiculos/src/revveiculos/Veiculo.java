package revveiculos;

public abstract class Veiculo {
    //atributos
    private String marca;
    private String modelo;
    private TipoVeiculo tipo;
    private double preco;
    private char combustivel;//a = alcool, g= gasolina, d=dieseil
    private int ano;

    public Veiculo(String marca, String modelo, TipoVeiculo tipo, double preco, char combustivel, int ano) {
        this.marca = marca;
        this.modelo = modelo;
        this.tipo = tipo;
        this.preco = preco;
        this.combustivel = combustivel;
        this.ano = ano;
    }
    
    //setters and getters
    public int getAno() {
        return ano;
    }

    public void setAno(int ano) {
        this.ano = ano;
    }

    public char getCombustivel() {
        return combustivel;
    }

    public void setCombustivel(char combustivel) {
        this.combustivel = combustivel;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public double getPreco() {
        return preco;
    }

    public void setPreco(double preco) {
        this.preco = preco;
    }

    public TipoVeiculo getTipo() {
        return tipo;
    }

    public void setTipo(TipoVeiculo tipo) {
        this.tipo = tipo;
    }
    //métodos
    public double obterPreco(){
        return 0.0;
    }
}
