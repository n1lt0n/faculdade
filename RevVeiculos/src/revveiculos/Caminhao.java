package revveiculos;

public class Caminhao extends Veiculo {
    private int capacidade;
    private int numEixos;
    private int categoria;

    public Caminhao(String marca, String modelo, TipoVeiculo tipo, double preco, char combustivel, int ano, int capacidade, int numEixos, int categoria) {
        super(marca, modelo, tipo, preco, combustivel, ano);
        this.capacidade = capacidade;
        this.numEixos = numEixos;
        this.categoria = categoria;
    }
    
    public int getCapacidade() {
        return capacidade;
    }

    public void setCapacidade(int capacidade) {
        this.capacidade = capacidade;
    }

    public int getCategoria() {
        return categoria;
    }

    public void setCategoria(int categoria) {
        this.categoria = categoria;
    }

    public int getNumEixos() {
        return numEixos;
    }

    public void setNumEixos(int numEixos) {
        this.numEixos = numEixos;
    }
    @Override
    public double obterPreco() {
        return super.getPreco()+(super.getPreco()*0.02);
    }

}
