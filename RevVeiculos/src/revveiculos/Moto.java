package revveiculos;

public class Moto extends Veiculo{
    private String tipoFreio;
    private String estilo;
    private String tipoMotor;

    public Moto(String marca, String modelo, TipoVeiculo tipo, double preco, char combustivel, int ano, String tipoFreio, String estilo, String tipoMotor) {
        super(marca, modelo, tipo, preco, combustivel, ano);
        this.tipoFreio = tipoFreio;
        this.estilo = estilo;
        this.tipoMotor = tipoMotor;
    }
    
    //setters and getters
    public String getEstilo() {
        return estilo;
    }

    public void setEstilo(String estilo) {
        this.estilo = estilo;
    }

    public String getTipoFreio() {
        return tipoFreio;
    }

    public void setTipoFreio(String tipoFreio) {
        this.tipoFreio = tipoFreio;
    }

    public String getTipoMotor() {
        return tipoMotor;
    }

    public void setTipoMotor(String tipoMotor) {
        this.tipoMotor = tipoMotor;
    }

    @Override
    public double obterPreco(){
        double aux;
        aux=super.getPreco() - (super.getPreco()*0.05);
        return aux;
    }

}
