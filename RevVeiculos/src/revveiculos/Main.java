package revveiculos;

import javax.swing.JOptionPane;

public class Main {

    public static void main(String[] args) {
        Veiculo teste[] = new Veiculo[3];
        teste[0] = new Moto("Honda", "2008", TipoVeiculo.MOTO, 2005.50, 'G', 2008, "Disco", "esportiva", "4 tempos");
        teste[1] = new Caminhao("Volvo", "2011", TipoVeiculo.MOTO, 150000.0, 'D', 2011, 10000, 8, 0);
        JOptionPane.showMessageDialog(null, "Preco: "+teste[0].getPreco(),"resultado",JOptionPane.INFORMATION_MESSAGE);
        JOptionPane.showMessageDialog(null, "Preco: "+teste[1].getPreco(),"resultado",JOptionPane.INFORMATION_MESSAGE);
    }

}
