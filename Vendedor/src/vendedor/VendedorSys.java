/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package vendedor;

/**
 *
 * @author fcg.1155376925
 */
public class VendedorSys {
    private int codProduto;
    private double valorUnitario;
    private int quantidade;

    public VendedorSys(int cp, double vu, int q){
        this.codProduto=cp;
        this.valorUnitario=vu;
        this.quantidade=q;
    }

    protected double calc(){
        return this.valorUnitario*this.quantidade;
    }

    public String resultado1(){
        return "Valor da Compra = "+calc();
    }

    public String resultado2(double desc){
        return resultado1()+"\nDesconto = "+(calc()*desc/100)+ "\nValor da Compra com Desconto = "+(calc()-(calc()*desc/100));
    }
}
