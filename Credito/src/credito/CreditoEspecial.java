package credito;

public class CreditoEspecial {
    private double saldoMedio;

    public CreditoEspecial(double sm) {
        this.saldoMedio=sm;
    }

    public String resultado(){
        if(this.saldoMedio<201) {
            return "Saldo: "+this.saldoMedio+"\nNenhum Crédito";
        } else {
            if(this.saldoMedio<401 && this.saldoMedio>200) {
                return "Saldo: "+this.saldoMedio+"\nCrédito de 20% \nCrédito: "+(this.saldoMedio*20/100);
            } else {
                if(this.saldoMedio<601 && this.saldoMedio>400){
                    return "Saldo: "+this.saldoMedio+"\nCrédito de 30% \nCrédito: "+(this.saldoMedio*30/100);
                } else {
                    if(this.saldoMedio>599){
                        return "Saldo: "+this.saldoMedio+"\nCrédito de 40% \nCrédito: "+(this.saldoMedio*40/100);
                    } else {
                        return "error";
                    }
                }
            }
        }
    }
}
