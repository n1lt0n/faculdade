package maior;

public class bigger {
    //atributos
    private int a;
    private int b;
    private int c;
    //métodos
    public bigger(int n1, int n2, int n3){
        this.a=n1;
        this.b=n2;
        this.c=n3;
    }
    public String moreThan() {
        int maior=this.a;
        if (maior<this.b) {
            maior=this.b;
        } else {
            if(maior<this.c){
                maior=this.c;
            }
        }
        return "O valor: "+maior+", é o maior!";
    }
}
