package study;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

@SuppressWarnings("serial")
public class Swingera extends JFrame implements ActionListener {
	//constantes
	private static final int width = 800;
	private static final int height = 600;
	//variáveis auxiliares
	private String itens[]= new String[3];
	//definindo componentes
	//container
	JFrame frame;
	//rotulos
	JLabel jLTipoCalculo;
	JLabel jLNotas;
	JLabel jLSexo;
	//combobox
	JComboBox jCTipoCalculo;
	//textfield
	JTextField jTNotas01;
	JTextField jTNotas02;
	//radiobutton
	JRadioButton jRMasc;
	JRadioButton jRFem;
	ButtonGroup bGGrupo;
	//checkbox
	JCheckBox jChMostrarMen;
	//botões
	JButton jBCalcular;
	JButton jBSair;
	
	public Swingera(){
		init();
	}

	@SuppressWarnings("static-access")
	private void init() {
		//inicializando vetor "Ponderada 40% e 60%","Ponderada 30% e 70%","Aritmética"
		itens[0]="Ponderada 40% e 60%";
		itens[1]="Ponderada 30% e 70%";
		itens[2]="Aritmética";
		//inicializando container
		frame = new JFrame();
		frame.setSize(800, 600);
		frame.setDefaultCloseOperation(EXIT_ON_CLOSE);
		frame.setDefaultLookAndFeelDecorated(rootPaneCheckingEnabled);
		frame.setLayout(new GridLayout(5,2));
		
		//inicializando componentes 
		jLTipoCalculo = new JLabel("Tipo do Cálculo:");
		jLNotas = new JLabel("Notas:");
		jLSexo = new JLabel("Sexo");
		jCTipoCalculo = new JComboBox(itens);
		jCTipoCalculo.setMaximumRowCount(3);
		jTNotas01 = new JTextField(10);
		jTNotas02 = new JTextField(10);
		jRMasc = new JRadioButton();
		jRFem = new JRadioButton();
		bGGrupo = new ButtonGroup();
		bGGrupo.add(jRFem);
		bGGrupo.add(jRMasc);
		jRMasc.addActionListener(this);
		jRFem.addActionListener(this);
		jChMostrarMen = new JCheckBox("Mostrar Mensagem");
		jBCalcular = new JButton("Calcular");
		jBSair = new JButton("Sair");
		
		//adicionando listeners
		jBCalcular.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				executarCalculo();
				
			}
		});
		jBSair.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// saida do programa
				System.exit(0);
			}
		});
		
		//adicionando elementos ao container
		frame.getContentPane().add(jLTipoCalculo);
		frame.getContentPane().add(jCTipoCalculo);
		frame.getContentPane().add(jLNotas);
		frame.getContentPane().add(jTNotas01);
		frame.getContentPane().add(jTNotas02);
		frame.getContentPane().add(jLSexo);
		frame.getContentPane().add(jRFem);
		frame.getContentPane().add(jRMasc);
		frame.getContentPane().add(jChMostrarMen);
		frame.getContentPane().add(jBCalcular);
		frame.getContentPane().add(jBSair);
                frame.pack();
                frame.setVisible(true);
		
	}

	protected void executarCalculo() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		
	}

}