create table Chamados {
	idChamados integer auto_increment not null primary key,
	Chamado long not null,
	Descricao text not null,
	DataHoraInicio DateTime not null,
	DataHoraFim DateTime,
	Historico text
	}
