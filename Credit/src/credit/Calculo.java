/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package credit;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author n1lt0n
 */
public class Calculo {
    File file;
    FileReader reader;
    BufferedReader leitor;

    private void abreArq(String caminho){
        file = new File(caminho);
        if(!file.exists()){
            JOptionPane.showMessageDialog(null, "Arquivo não existe!", "Erro", JOptionPane.ERROR_MESSAGE);
        }else {
            if(!file.isFile()){
                JOptionPane.showMessageDialog(null, "Arquivo selecionado não é válido", "Erro", JOptionPane.ERROR_MESSAGE);
            } else {
                try {
                    reader = new FileReader(file);
                } catch (FileNotFoundException ex) {
                    Logger.getLogger(Calculo.class.getName()).log(Level.SEVERE, null, ex);
                }
                leitor = new BufferedReader(reader);
            }
        }
    }
    private void fechaArq(){
        try {
            leitor.close();
        } catch (IOException ex) {
            Logger.getLogger(Calculo.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            reader.close();
        } catch (IOException ex) {
            Logger.getLogger(Calculo.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public String exibirFirst(){
        String linha=null;
        String linha2=null;
        abreArq("/home/n1lt0n/Documentos/entradaP.txt");
        try {
            linha = leitor.readLine();
            linha2 = leitor.readLine();
        } catch (IOException ex) {
            Logger.getLogger(Calculo.class.getName()).log(Level.SEVERE, null, ex);
        }
        fechaArq();
        return "Primeira linha do Arquivo: \n"+linha+"\n"+linha2;
    }
}
