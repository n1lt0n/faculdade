/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package threadss;

/**
 *
 * @author Nilton
 */
public class Produtor extends Thread{
    private Estoque estoque;
    
    public Produtor(){}
    public Produtor(Estoque estoque){
        this.estoque=estoque;
    }
    
//    public void produzir(){
//        synchronized(estoque) {
//            //insere recurso no estoque
//           Recurso recurso = new Recurso((int)(Math.random() * 10000));
//            this.estoque.getConteudo().add(recurso);
//
//            
//            System.out.println("+ "+this.getName()+"\t -> Recurso Produzido: "+recurso);
//            
//            //notifica os consumidores que o estoque já foi atualizado
//            estoque.notifyAll();
//        }
//    }
    @Override
    @SuppressWarnings("CallToThreadDumpStack")
    public void run(){
        int count=0;
        while(count<5000){
            synchronized(estoque) {
            //insere recurso no estoque
           Recurso recurso = new Recurso((int)(Math.random() * 10000));
            this.estoque.getConteudo().add(recurso);

            
            System.out.println("+ "+this.getName()+"\t -> Recurso Produzido: "+recurso);
            
            //notifica os consumidores que o estoque já foi atualizado
            estoque.notifyAll();
            }
            
            try {
                Thread.sleep((long)Math.random()*Configuracoes.MAX_TIME_TO_SLEEP);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            count++;
        }
    }
    public Estoque getEstoque(){
        return estoque;
    }
    public void setEstoque(Estoque estoque){
        this.estoque=estoque;
    }
}
