/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package threadss;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 *
 * @author Nilton
 */
public class Threadss {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //repositório de recursos
        Estoque estoque=new Estoque();
        
        //ExecutorService app = Executors.newFixedThreadPool(3);
        
        //produtor
        Produtor produtor = new Produtor(estoque);
        produtor.setName("Produtor");
        
        //consumidores
        Consumidor consumidor1=new Consumidor(estoque);
        consumidor1.setName("Consumidor_1");
        
        Consumidor consumidor2=new Consumidor(estoque);
        consumidor2.setName("Consumidor_2");
        consumidor1.setPriority(Thread.MIN_PRIORITY);
        
        //burn baby burn
        produtor.start();
        consumidor1.start();
        consumidor2.start();
        
//        try {
//            app.execute(produtor);
//            app.execute(consumidor1);
//            //app.execute(consumidor2);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        finally{
//            app.shutdown();
//        }
    }
}