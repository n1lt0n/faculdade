/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package datahoraformat;

/**
 *
 * @author fcg.1155376925
 */
public class DataHora {
    private int dia;
    private int mes;
    private int ano;
    private int hora;
    private int minuto;
    private int segundo;

    DataHora(int d, int me, int a, int h, int mi, int s){
        setDia(d);
        setMes(me);
        setAno(a);
        setHora(h);
        setMinuto(mi);
        setSegundo(s);
    }

    public String met1(){
        return getDia()+"/"+getMes()+"/"+getAno();
    }
    public String met2(){
        return getHora()+":"+getMinuto()+":"+getSegundo();
    }
    public String met3(){
        return met1()+" "+met2();
    }

    public int getAno() {
        return ano;
    }

    public int getDia() {
        return dia;
    }

    public int getHora() {
        return hora;
    }

    public int getMes() {
        return mes;
    }

    public int getMinuto() {
        return minuto;
    }

    public int getSegundo() {
        return segundo;
    }

    public void setAno(int ano) {
        this.ano = ano;
    }

    public void setDia(int dia) {
        this.dia = dia;
    }

    public void setHora(int hora) {
        this.hora = hora;
    }

    public void setMes(int mes) {
        this.mes = mes;
    }

    public void setMinuto(int minuto) {
        this.minuto = minuto;
    }

    public void setSegundo(int segundo) {
        this.segundo = segundo;
    }


}
