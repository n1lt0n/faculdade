<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>Controle de Escalas</title>
        <link href="css/main.css" rel="stylesheet" type="text/css"/>
        <script src="js/jquery-1.11.1.min.js" type="text/javascript"></script>
        <script src="js/application.js" type="text/javascript"></script>
    </head>
    <body>
        <div class="container">
            <div class="header">
                <div class="logo">
                    <p>Controle de Escalas</p>
                </div>
                <div class="menu">
                    <ul class="menulist">
                        <li>Home</li>
                        <li>Funcionários</li>
                        <li>Horários</li>
                    </ul>
                </div>
            </div>
            <div class="content" >
                <table>
                    <thead>
                        <td>Funcionários</td>
                        <td>29/06</td>
                        <td>30/06</td>
                        <td>01/07</td>
                        <td>02/07</td>
                        <td>03/07</td>
                        <td>04/07</td>
                        <td>05/07</td>
                    </thead>
                    <tr>
                        <td>Jéssica França de Azevedo</td>
                        <td>07:00 - 16:00</td>
                        <td>07:00 - 16:00</td>
                        <td>07:00 - 16:00</td>
                        <td>07:00 - 16:00</td>
                        <td>07:00 - 16:00</td>
                        <td>07:00 - 16:00</td>
                        <td>07:00 - 16:00</td>
                    </tr>
                    <tr>
                        <td>Kayo Cezar Cisz Mariano</td>
                        <td>08:00 - 17:00</td>
                        <td>08:00 - 17:00</td>
                        <td>08:00 - 17:00</td>
                        <td>08:00 - 17:00</td>
                        <td>08:00 - 17:00</td>
                        <td>08:00 - 17:00</td>
                        <td>08:00 - 17:00</td>
                    </tr>
                </table>
                <div class="funcionarios">
                    
                </div>
            </div>
            <div class="footer">
                <p>Criado por <a href="mailto:nilton_tadeu@yahoo.com.br">Nilton Tadeu Ferreira Filho</a></p>
            </div>
        </div>
        <?php
        
        ?>
    </body>
</html>
