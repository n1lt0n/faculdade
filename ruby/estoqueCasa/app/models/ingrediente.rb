class Ingrediente < ActiveRecord::Base
	belongs_to :Receita
	belongs_to :Unidade
	belongs_to :Produto
end
