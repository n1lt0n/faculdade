class Produto < ActiveRecord::Base
	has_many :Estoque
	has_many :Ingrediente
	belongs_to :Unidade
	belongs_to :Moeda
end
