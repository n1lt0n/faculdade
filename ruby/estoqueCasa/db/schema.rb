# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20151016195700) do

  create_table "estoques", force: true do |t|
    t.date     "dataCompraEstoque"
    t.decimal  "quantidadeProduto"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "ingredientes", force: true do |t|
    t.float    "quantidadeIngrediente"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "moedas", force: true do |t|
    t.string   "nomeMoeda"
    t.string   "siglaMoeda"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "produtos", force: true do |t|
    t.string   "nomeProduto"
    t.string   "marcaProduto"
    t.float    "precoUnitarioProduto"
    t.float    "pesoProduto"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "receita", force: true do |t|
    t.string   "tituloReceita"
    t.string   "descricaoReceita"
    t.string   "autoraReceita"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "unidades", force: true do |t|
    t.string   "nomeUnidade"
    t.string   "siglaUnidade"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end
