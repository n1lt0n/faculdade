class CreateUnidades < ActiveRecord::Migration
  def change
    create_table :unidades do |t|
      t.string :nomeUnidade
      t.string :siglaUnidade

      t.timestamps
    end
  end
end
