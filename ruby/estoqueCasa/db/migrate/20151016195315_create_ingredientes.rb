class CreateIngredientes < ActiveRecord::Migration
  def change
    create_table :ingredientes do |t|
      t.float :quantidadeIngrediente

      t.timestamps
    end
  end
end
