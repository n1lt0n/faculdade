class CreateProdutos < ActiveRecord::Migration
  def change
    create_table :produtos do |t|
      t.string :nomeProduto
      t.string :marcaProduto
      t.float :precoUnitarioProduto
      t.float :pesoProduto

      t.timestamps
    end
  end
end
