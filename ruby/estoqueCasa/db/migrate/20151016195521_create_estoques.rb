class CreateEstoques < ActiveRecord::Migration
  def change
    create_table :estoques do |t|
      t.date :dataCompraEstoque
      t.decimal :quantidadeProduto

      t.timestamps
    end
  end
end
