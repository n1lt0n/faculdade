class CreateMoedas < ActiveRecord::Migration
  def change
    create_table :moedas do |t|
      t.string :nomeMoeda
      t.string :siglaMoeda

      t.timestamps
    end
  end
end
