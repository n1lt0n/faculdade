class CreateReceita < ActiveRecord::Migration
  def change
    create_table :receita do |t|
      t.string :tituloReceita
      t.string :descricaoReceita
      t.string :autoraReceita

      t.timestamps
    end
  end
end
