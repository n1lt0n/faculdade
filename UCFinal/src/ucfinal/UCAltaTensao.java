    package ucfinal;

    public class UCAltaTensao extends UnidadeConsumidora{

        //Atributos
        private int leituraAtual;
        private int leituraAnterior;

        //Construtor
        public UCAltaTensao(int cu,String np,String eu,double vt){
            super(cu, np, eu, vt);

        }
        //setter e getter
        @Override
       public int getLeituraAnterior() {
            return leituraAnterior;
        }
        @Override
        public void setLeituraAnterior(int leituraAnterior) {
            this.leituraAnterior = leituraAnterior;
        }
        @Override
        public int getLeituraAtual() {
            return leituraAtual;
        }
        @Override
        public void setLeituraAtual(int leituraAtual) {
            this.leituraAtual = leituraAtual;
        }
        //métodos adicionais
        @Override
        public Double ValorAPagar(){
            double aux;
            if (this.leituraAtual<this.leituraAnterior) {
                aux=((10000+this.leituraAtual)-this.leituraAnterior)*super.getValorTarifa();
                aux = aux+(aux*0.3);
                return aux;
            } else {
                aux=(this.leituraAtual-this.leituraAnterior)*super.getValorTarifa();
                aux = aux+(aux*0.3);
                return aux;
            }
        }

    }