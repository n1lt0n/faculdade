package planosaude;

import javax.swing.JOptionPane;

public class Saude {

    //constantes
    public final static double at10 = 50.0;
    public final static double a1029 = 75.0;
    public final static double a2945 = 130.0;
    public final static double a4559 = 180.0;
    public final static double a5965 = 270.0;
    public final static double m65 = 400.0;
    //atributos
    private String nome;
    private int idade;
    private double renda;
    private int plano; //0-open 1-standard 2-gold

    public Saude(String n, int i, double r, int p) {
        this.nome = n;
        this.idade = i;
        this.renda = r;
        this.plano = p;
    }

    public double verificaPlano(){
        switch(this.plano) {
            case 0: return -0.15;
            case 1: return 0;
            case 2: return 0.215;
            default: return 100;
        }
    }

    public boolean validaRenda(){
        switch(this.plano){
            case 0: if(this.renda>1500.0) {
                return false;
            } else
                return true;
            case 1: if(this.renda<=1500.0 || this.renda>=4500.0) {
                return false;
            } else
                return true;
            case 2: if(this.renda<4500.0){
                return false;
            }else
                return true;
            default: return true;
        }
    }

    public double verificaIdade() {
        if (idade<=10){
            return at10;
        } else
            if(idade>=11 && idade<=29){
                return a1029;
            } else {
                if (idade>=30 && idade<=45){
                    return a2945;
                } else {
                    if (idade>=46 && idade<=59) {
                        return a4559;
                    } else {
                        if (idade>=60 && idade<=65) {
                            return a5965;
                        } else {
                            return m65;
                        }
                    }
                }
            }
    }
    public String resultado() {
        if (!validaRenda()){
            return "Renda Incompativel com plano!";
        }else {
            //JOptionPane.showMessageDialog(null, "VerificaIdade: "+verificaIdade()+"\nVerificaplano: "+verificaPlano()+"Plano: "+this.plano, "teste", JOptionPane.INFORMATION_MESSAGE);
            return "Nome: "+this.nome+"\nIdade: "+this.idade+"\nValor a Pagar: "+(verificaIdade()+(verificaIdade()*verificaPlano()));
        }
    }
}
