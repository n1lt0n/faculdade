/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package planosaude;

import javax.swing.JOptionPane;

/**
 *
 * @author fcg.1155376925
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //FrontEnd fe = new FrontEnd();
        //fe.setVisible(true);
        String n = JOptionPane.showInputDialog(null, "Entre com o Nome","Nome",JOptionPane.PLAIN_MESSAGE);
        int i = Integer.parseInt(JOptionPane.showInputDialog(null, "Entre com a Idade","Idade",JOptionPane.PLAIN_MESSAGE));
        double r = Double.parseDouble(JOptionPane.showInputDialog(null, "Entre com a Renda","Renda",JOptionPane.PLAIN_MESSAGE));
        int p = Integer.parseInt(JOptionPane.showInputDialog(null, "Entre com o Plano seguindo esta convenção(0-Open, 1-Standard, 2-Gold","Plano",JOptionPane.PLAIN_MESSAGE));
        Saude ss = new Saude(n, i, r, p);
        JOptionPane.showMessageDialog(null, ss.resultado(), "Resultado", JOptionPane.INFORMATION_MESSAGE);
    }

}
