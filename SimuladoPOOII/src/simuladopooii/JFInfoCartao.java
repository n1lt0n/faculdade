/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package simuladopooii;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;

/**
 *
 * @author nilton
 */
public class JFInfoCartao extends JFrame {

    private JLabel jLBandeira;
    private JLabel jLTipoCred;
    private JComboBox jCoBandeira;
    private JComboBox jCoTipoCred;
    private JButton jBCalcular;
    private JButton jBVoltar;
    private JFrame frame;
    //variaveis auxiliares
    private double vc;

    public JFInfoCartao() {
        init();
    }

    JFInfoCartao(double valorCompra) {
        init();
        vc = valorCompra;
    }

    private void init() {
        frame = new JFrame("Pagamento Cartão");
        frame.setSize(320, 240);
        frame.setDefaultCloseOperation(HIDE_ON_CLOSE);
        frame.setLayout(new GridLayout(3, 2));

        jLBandeira = new JLabel("Bandeira:");
        jLTipoCred = new JLabel("Tipo de Crédito/Débito:");

        jCoBandeira = new JComboBox(new DefaultComboBoxModel(new String[]{"VISA", "MASTERCARD"}));
        jCoTipoCred = new JComboBox(new DefaultComboBoxModel(new String[]{"CRÉDITO", "DÉBITO"}));

        jBCalcular = new JButton("Calcular");
        jBCalcular.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                execCalc();
            }
        });
        jBVoltar = new JButton("Voltar");
        jBVoltar.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                frame.setVisible(false);
            }
        });

        frame.getContentPane().add(jLBandeira);
        frame.getContentPane().add(jCoBandeira);
        frame.getContentPane().add(jLTipoCred);
        frame.getContentPane().add(jCoTipoCred);
        frame.getContentPane().add(jBCalcular);
        frame.getContentPane().add(jBVoltar);
        pack();
    }

    public void iniciar() {
        frame.setVisible(true);
    }

    private void execCalc() {
        PagamentoCartao pc = new PagamentoCartao(vc,jCoTipoCred.getSelectedIndex(),jCoBandeira.getSelectedItem().toString());
    }
}
