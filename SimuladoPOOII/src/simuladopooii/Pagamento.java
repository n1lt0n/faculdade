
package simuladopooii;

public class Pagamento {
    private double valor;
    private double desconto;
    
    public Pagamento(){}
    public Pagamento(double v){
        valor=v;
        calcDesconto();
    }
    
    public void setValor(double v){
        valor=v;
    }
    
    public double getValor(){
        return valor;
    }

    public double getDesconto() {
        return desconto;
    }
    
    private void calcDesconto(){
        if (valor>=80.0 && valor<=130.0){
            desconto=5.0;
        }else{
            if(valor<=80.0){
                desconto=0;
            } else {
                desconto=7.5;
            }
        }
    }
    
    public double calcValor(){
        return valor-(valor*(desconto/100));
    }
    
    public String getFormaPgto(){
        return "À Vsita";
    }
}
