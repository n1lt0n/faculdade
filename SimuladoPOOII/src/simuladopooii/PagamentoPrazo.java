package simuladopooii;

public class PagamentoPrazo  extends Pagamento{
    private int dias;
    private double juros;

    public int getDias() {
        return dias;
    }

    public void setDias(int dias) {
        this.dias = dias;
    }

    public double getJuros() {
        return juros;
    }

    public void setJuros(double juros) {
        this.juros = juros;
    }
    public double calcJuros(){
        switch(dias) {
            case 1 : 
                return 0.02;
            case 2 : 
                return 0.02;
            case 3 :
                return 0.02;
            case 4 :
                return 0.02;
            case 5 : 
                return 0.02;
            case 6 : 
                return 0.02;
            case 7 :
                return 0.02;
            case 8 :
                return 0.05;
            case 9 : 
                return 0.05;
            case 10 : 
                return 0.05;
            case 11 :
                return 0.05;
            case 12 :
                return 0.05;
            case 13 : 
                return 0.05;
            case 14 : 
                return 0.05;
            default :
                return 0.05+((dias-14)*0.0022);
        }
    }
    @Override
    public double calcValor(){
        return getValor()*calcJuros();
    }
    @Override
    public String getFormaPgto(){
        return "Cheque ("+dias+" dias)";
    }
}
