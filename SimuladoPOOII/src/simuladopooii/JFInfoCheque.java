/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package simuladopooii;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

/**
 *
 * @author Nilton
 */
public class JFInfoCheque extends JFrame{
    private JLabel jLDias;
    private JTextField jTDias;
    private JButton jBCalcular;
    private JButton jBVoltar;
    private JFrame frame;
    
    public JFInfoCheque(){
        init();
    }

    private void init() {
        
        frame = new JFrame("Pagamento com Cheque");
        frame.setSize(320, 120);
        frame.setDefaultCloseOperation(HIDE_ON_CLOSE);
        frame.setLayout(new GridLayout(2, 2));
        
        jLDias = new JLabel("Dias:");
        jTDias = new JTextField();
        jBCalcular = new JButton("Calcular");
        jBCalcular.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                throw new UnsupportedOperationException("Not supported yet.");
            }
        });
        jBVoltar = new JButton("Voltar");
        jBVoltar.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                frame.setVisible(false);
            }
        });
        
        frame.getContentPane().add(jLDias);
        frame.getContentPane().add(jTDias);
        frame.getContentPane().add(jBCalcular);
        frame.getContentPane().add(jBVoltar);
        pack();
        
    }
    public void iniciar(){
        frame.setVisible(true);
    }
}
