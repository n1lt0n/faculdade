/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package simuladopooii;

/**
 *
 * @author nilton
 */
public class PagamentoCartao extends Pagamento{
    private double taxa;
    private String bandeira;
    private int tipoDeCred;//0=credito 1=debito
    
    public PagamentoCartao(){}
    public PagamentoCartao(double v, int t, String b){
        super.setValor(v);
        bandeira=b;
        tipoDeCred=t;
    }

    public String getBandeira() {
        return bandeira;
    }

    public void setBandeira(String bandeira) {
        this.bandeira = bandeira;
    }

    public double getTaxa() {
        return taxa;
    }

    public void setTaxa(double taxa) {
        this.taxa = taxa;
    }

    public int getTipoDeCred() {
        return tipoDeCred;
    }

    public void setTipoDeCred(int tipoDeCred) {
        this.tipoDeCred = tipoDeCred;
    }
    public double calcBandeira(){
        double resultado=super.getValor();
        if (tipoDeCred==0){//0=credito
            if(bandeira.equalsIgnoreCase("VISA")){
                resultado=resultado+(resultado*0.032);
            }
        }
        else {//1=debito
            if (bandeira.equalsIgnoreCase("VISA")){
                resultado=resultado+(resultado*0.023);
            }
            else {
                resultado=resultado-(resultado*0.01);
            }
        }
        return resultado;
    }
    @Override
    public double calcValor(){
        return calcBandeira();
    }
    
    @Override
    public String getFormaPgto(){
        String out;
        out=bandeira;
        if(tipoDeCred==0){
            out=out+" / Crédito";
        }else {
            out=out+" / Débito";
        }
        return out;
    }
}