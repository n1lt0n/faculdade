/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package simuladopooii;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author nilton
 */
public class JFSimuladoPOOII extends JFrame {

    private JLabel jLValorCompra;
    private JLabel jLFormaPgto;
    private JLabel jLValorTotal;
    private JTextField jTValorCompra;
    private JTextField jTValorTotal;
    private JTextField jTDesconto;
    private JCheckBox jChMostrarMens;
    private JComboBox jCoFormaPgto;
    private JButton jBCalcular;
    private JButton jBSair;
    private JFrame frame;

    public JFSimuladoPOOII() {
        init();
    }

    private void init() {
        frame = new JFrame();
        frame.setSize(320, 240);
        frame.setDefaultCloseOperation(EXIT_ON_CLOSE);
        frame.setLayout(new GridLayout(5, 2));

        jLFormaPgto = new JLabel("Forma de Pagamento:");
        jLValorCompra = new JLabel("Valor da Compra:");
        jLValorTotal = new JLabel("Valor Total:");

        jTDesconto = new JTextField();
        jTValorCompra = new JTextField();
        jTValorTotal = new JTextField();
        jTValorTotal.setEnabled(false);
        jTValorTotal.setBackground(Color.black);
        jTDesconto.setEnabled(false);
        jTDesconto.setBackground(Color.black);

        jChMostrarMens = new JCheckBox("Mostrar Mensagem");
        jChMostrarMens.setSelected(true);

        jCoFormaPgto = new JComboBox(new DefaultComboBoxModel(new String[]{"À Vista", "Cartão Débito/Crédito", "A Prazo com Cheque"}));

        jBCalcular = new JButton("Calcular");
        jBCalcular.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                execCalc();
            }
        });
        jBSair = new JButton("Sair");
        jBSair.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });
        frame.getContentPane().add(jLValorCompra);
        frame.getContentPane().add(jTValorCompra);
        frame.getContentPane().add(jLFormaPgto);
        frame.getContentPane().add(jCoFormaPgto);
        frame.getContentPane().add(jLValorTotal);
        frame.getContentPane().add(jTValorTotal);
        frame.getContentPane().add(jChMostrarMens);
        frame.getContentPane().add(jTDesconto);
        frame.getContentPane().add(jBCalcular);
        frame.getContentPane().add(jBSair);
        frame.pack();
    }

    public void iniciar() {
        frame.setVisible(true);
    }

    private void error(){
        JOptionPane.showMessageDialog(null, "Valor da Compra é um campo de \nPreenchimento Obrigatório", "Error", JOptionPane.ERROR_MESSAGE);
    }
    private void execCalc() {
        switch(jCoFormaPgto.getSelectedIndex()){
            case 0:
                Pagamento pg;
                if( jTValorCompra.getText().equals("")){
                    error();
                }else {
                    pg=new Pagamento(Double.parseDouble(jTValorCompra.getText()));
                    setJTDesconto(pg.getDesconto(), "Desconto");
                    setJTValorTotal(pg.calcValor());
                }
                break;
            case 1 :
                JFInfoCartao ic;
                if( jTValorCompra.getText().equals("")){
                    error();
                }else {
                    ic = new JFInfoCartao(Double.parseDouble(jTValorCompra.getText()));
                    ic.iniciar();
                }
                break;
            case 2 :
                JFInfoCheque ich;
                if( jTValorCompra.getText().equals("")){
                    error();
                }else {
                    ich = new JFInfoCheque();
                    ich.iniciar();
                }
        }
    }
    
    public void setJTDesconto(double desc, String descAcres){
        jTDesconto.setText(""+descAcres+" de "+desc+"%");
    }
    public void setJTValorTotal(double vt){
        jTValorTotal.setText("R$ "+vt);
    }
//    public static void main(String args[]){
//        java.awt.EventQueue.invokeLater(new Runnable() {
//
//            @Override
//            public void run() {
//                new JFSimuladoPOOII().setVisible(true);
//            }
//        });
//    }
}