package ucfase3;

import javax.swing.JOptionPane;

public class JFUCFase3 extends javax.swing.JFrame {
    public static final double tar = 0.34;
    private UnidadeConsumidora uc;
    public JFUCFase3() {
        initComponents();
        //setando paineis visiveis
        jPEntrada.setVisible(true);
        jPSaved.setVisible(false);
        jPLeFa.setVisible(false);
        jPChTar.setVisible(false);
        //setando menus habilitados
        jMLeFa.setEnabled(false);
        jMTarifa.setEnabled(false);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPSaved = new javax.swing.JPanel();
        jPLeFa = new javax.swing.JPanel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jTLeAn = new javax.swing.JTextField();
        jTLeAtu = new javax.swing.JTextField();
        jButton2 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLRCu = new javax.swing.JLabel();
        jLRNp = new javax.swing.JLabel();
        jLREu = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        jLabel10 = new javax.swing.JLabel();
        jPChTar = new javax.swing.JPanel();
        jLabel9 = new javax.swing.JLabel();
        jTTarA = new javax.swing.JTextField();
        jButton4 = new javax.swing.JButton();
        jButton5 = new javax.swing.JButton();
        jPEntrada = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jTCu = new javax.swing.JTextField();
        jTNp = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTAEu = new javax.swing.JTextArea();
        jButton1 = new javax.swing.JButton();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMLeFa = new javax.swing.JMenuItem();
        jMTarifa = new javax.swing.JMenuItem();
        jMenuItem3 = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("SisUC");
        setBounds(new java.awt.Rectangle(200, 200, 0, 0));

        jLabel7.setText("Leitura Anterior:");

        jLabel8.setText("Leitura Atual:");

        jButton2.setText("Gerar Fatura");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jButton3.setText("Fechar Leitura");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPLeFaLayout = new javax.swing.GroupLayout(jPLeFa);
        jPLeFa.setLayout(jPLeFaLayout);
        jPLeFaLayout.setHorizontalGroup(
            jPLeFaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPLeFaLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPLeFaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPLeFaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPLeFaLayout.createSequentialGroup()
                            .addComponent(jLabel7)
                            .addGap(260, 260, 260))
                        .addGroup(jPLeFaLayout.createSequentialGroup()
                            .addComponent(jLabel8)
                            .addGap(274, 274, 274)))
                    .addGroup(jPLeFaLayout.createSequentialGroup()
                        .addComponent(jButton3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)))
                .addGroup(jPLeFaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jTLeAtu)
                    .addComponent(jTLeAn)
                    .addComponent(jButton2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPLeFaLayout.setVerticalGroup(
            jPLeFaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPLeFaLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPLeFaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(jTLeAn, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPLeFaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8)
                    .addComponent(jTLeAtu, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 64, Short.MAX_VALUE)
                .addGroup(jPLeFaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton2)
                    .addComponent(jButton3))
                .addContainerGap())
        );

        jLabel4.setText("Código da Unidade Consumidora:");

        jLabel5.setText("Nome do Proprietário:");

        jLabel6.setText("Endereço da Unidade Consumidora:");

        jLRCu.setBackground(new java.awt.Color(255, 255, 255));
        jLRCu.setText("jLabel7");

        jLRNp.setBackground(new java.awt.Color(255, 255, 255));
        jLRNp.setText("jLabel8");

        jLREu.setBackground(new java.awt.Color(255, 255, 255));
        jLREu.setText("jLabel9");

        jLabel10.setFont(new java.awt.Font("Tahoma", 0, 18));
        jLabel10.setText("Dados da Unidade Consumidora");

        jLabel9.setText("Valor Unitária da Tarifa de Energia Elétrica:");

        jButton4.setText("Salvar");
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });

        jButton5.setText("Fechar Mudança da Tarifa");
        jButton5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton5ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPChTarLayout = new javax.swing.GroupLayout(jPChTar);
        jPChTar.setLayout(jPChTarLayout);
        jPChTarLayout.setHorizontalGroup(
            jPChTarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPChTarLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPChTarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPChTarLayout.createSequentialGroup()
                        .addComponent(jLabel9)
                        .addGap(165, 165, 165))
                    .addGroup(jPChTarLayout.createSequentialGroup()
                        .addComponent(jButton5)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)))
                .addGroup(jPChTarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jTTarA, javax.swing.GroupLayout.DEFAULT_SIZE, 65, Short.MAX_VALUE)
                    .addComponent(jButton4, javax.swing.GroupLayout.DEFAULT_SIZE, 65, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPChTarLayout.setVerticalGroup(
            jPChTarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPChTarLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPChTarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9)
                    .addComponent(jTTarA, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 91, Short.MAX_VALUE)
                .addGroup(jPChTarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton4)
                    .addComponent(jButton5))
                .addContainerGap())
        );

        javax.swing.GroupLayout jPSavedLayout = new javax.swing.GroupLayout(jPSaved);
        jPSaved.setLayout(jPSavedLayout);
        jPSavedLayout.setHorizontalGroup(
            jPSavedLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPSavedLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPSavedLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jPChTar, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel10, javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPSavedLayout.createSequentialGroup()
                        .addComponent(jLabel4)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 263, Short.MAX_VALUE)
                        .addComponent(jLRCu))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPSavedLayout.createSequentialGroup()
                        .addComponent(jLabel5)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 317, Short.MAX_VALUE)
                        .addComponent(jLRNp))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPSavedLayout.createSequentialGroup()
                        .addComponent(jLabel6)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 251, Short.MAX_VALUE)
                        .addComponent(jLREu))
                    .addComponent(jSeparator1, javax.swing.GroupLayout.DEFAULT_SIZE, 456, Short.MAX_VALUE))
                .addContainerGap())
            .addGroup(jPSavedLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPSavedLayout.createSequentialGroup()
                    .addGap(11, 11, 11)
                    .addComponent(jPLeFa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(11, Short.MAX_VALUE)))
        );
        jPSavedLayout.setVerticalGroup(
            jPSavedLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPSavedLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel10)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPSavedLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(jLRCu))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPSavedLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(jLRNp))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPSavedLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(jLREu))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPChTar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
            .addGroup(jPSavedLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPSavedLayout.createSequentialGroup()
                    .addContainerGap(121, Short.MAX_VALUE)
                    .addComponent(jPLeFa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap()))
        );

        jLabel1.setText("Código da Unidade Consumidora:");

        jLabel2.setText("Nome do Proprietário:");

        jLabel3.setText("Endereço da Unidade Consumnidora:");

        jTAEu.setColumns(20);
        jTAEu.setRows(5);
        jScrollPane1.setViewportView(jTAEu);

        jButton1.setText("Salvar");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPEntradaLayout = new javax.swing.GroupLayout(jPEntrada);
        jPEntrada.setLayout(jPEntradaLayout);
        jPEntradaLayout.setHorizontalGroup(
            jPEntradaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPEntradaLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPEntradaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPEntradaLayout.createSequentialGroup()
                        .addGroup(jPEntradaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1)
                            .addComponent(jLabel2)
                            .addComponent(jLabel3))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPEntradaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 275, Short.MAX_VALUE)
                            .addComponent(jTNp, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 275, Short.MAX_VALUE)
                            .addComponent(jTCu, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 275, Short.MAX_VALUE)))
                    .addComponent(jButton1, javax.swing.GroupLayout.Alignment.TRAILING))
                .addContainerGap())
        );
        jPEntradaLayout.setVerticalGroup(
            jPEntradaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPEntradaLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPEntradaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jTCu, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPEntradaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(jTNp, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPEntradaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel3)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 94, Short.MAX_VALUE)
                .addComponent(jButton1)
                .addContainerGap())
        );

        jMenu1.setText("Arquivo");

        jMLeFa.setText("Fornecer um Leitura e Gerar Fatura");
        jMLeFa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMLeFaActionPerformed(evt);
            }
        });
        jMenu1.add(jMLeFa);

        jMTarifa.setText("Alterar o Valor Unitário do KW/h");
        jMTarifa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMTarifaActionPerformed(evt);
            }
        });
        jMenu1.add(jMTarifa);

        jMenuItem3.setText("Sair");
        jMenuItem3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem3ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem3);

        jMenuBar1.add(jMenu1);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 476, Short.MAX_VALUE)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(jPSaved, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addGap(0, 0, Short.MAX_VALUE)
                    .addComponent(jPEntrada, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 0, Short.MAX_VALUE)))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 287, Short.MAX_VALUE)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(jPSaved, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addGap(0, 0, Short.MAX_VALUE)
                    .addComponent(jPEntrada, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 0, Short.MAX_VALUE)))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        if(jTCu.getText().equals("") || jTNp.getText().equals("") || jTAEu.getText().equals("")){
            JOptionPane.showMessageDialog(JFUCFase3.this, "Um ou mais campos obrigatórios não foram preenchidos!", "Error", JOptionPane.ERROR_MESSAGE);
        } else {
            //TODO: o campo codigo esta sendo convertido de texto para inteiro, é preciso utilizar um bloco trycatch para evitar crash do sistema
            uc = new UnidadeConsumidora(Integer.parseInt(jTCu.getText()),jTNp.getText(),jTAEu.getText(),tar);
            jMLeFa.setEnabled(true);
            jMTarifa.setEnabled(true);
            jLRCu.setText(""+uc.getCodigoUc());
            jLRNp.setText(uc.getNomeProprietario());
            jLREu.setText(uc.getEnderecoUc());
            jPEntrada.setVisible(false);
            jPSaved.setVisible(true);
            
        }
    }//GEN-LAST:event_jButton1ActionPerformed
  
    private void jMenuItem3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem3ActionPerformed
        System.exit(0);
    }//GEN-LAST:event_jMenuItem3ActionPerformed

    private void jMLeFaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMLeFaActionPerformed
        jPLeFa.setVisible(true);
        jPChTar.setVisible(false);
    }//GEN-LAST:event_jMLeFaActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        int aux;
        int error=0;
        if(jTLeAn.getText().equals("") || jTLeAtu.getText().equals("")){
            JOptionPane.showMessageDialog(JFUCFase3.this, "Um ou mais campos obrigatórios não foram preenchidos!", "Error", JOptionPane.ERROR_MESSAGE);
        } else {
            aux=Integer.parseInt(jTLeAn.getText());
            if(aux>=9999 || aux<0){
                JOptionPane.showMessageDialog(JFUCFase3.this, "Valores Incorretos na Leitura Anterior.\n O valor deve ser entre 0 e 9999", "Error", JOptionPane.ERROR_MESSAGE);
                jTLeAn.setText("");
                error++;
            }else {
                uc.setLeituraAnterior(aux);
                aux=Integer.parseInt(jTLeAtu.getText());
                if(aux>=9999 || aux<0){
                    JOptionPane.showMessageDialog(JFUCFase3.this, "Valores Incorretos na Leitura Atual.\n O valor deve ser entre 0 e 9999", "Error", JOptionPane.ERROR_MESSAGE);
                    jTLeAtu.setText("");
                    error++;
                } else {
                    uc.setLeituraAtual(aux);
                }
            }
            if(error==0){
                JOptionPane.showMessageDialog(null, "Código da Unidade Consumidora:"+uc.getCodigoUc()+"\nNome do Proprietário: "+uc.getNomeProprietario()+"\nEndereço da Unidade Consumidora: "+uc.getEnderecoUc()+"\nLeitura Anterior: "+uc.getLeituraAnterior()+"\nLeitura Atual: "+uc.getLeituraAtual()+"\nValor da Tarifa: "+uc.getValorTarifa()+"\nTotal a Pagar: "+uc.ValorAPagar(), "Fatura", JOptionPane.INFORMATION_MESSAGE);
            }
        }
        jPLeFa.setVisible(false);
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        jPLeFa.setVisible(false);
}//GEN-LAST:event_jButton3ActionPerformed

    private void jMTarifaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMTarifaActionPerformed
        jPLeFa.setVisible(false);
        jPChTar.setVisible(true);
    }//GEN-LAST:event_jMTarifaActionPerformed

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
        uc.setValorTarifa(Double.parseDouble(jTTarA.getText()));
        jPChTar.setVisible(false);
    }//GEN-LAST:event_jButton4ActionPerformed

    private void jButton5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton5ActionPerformed
        jPChTar.setVisible(false);
    }//GEN-LAST:event_jButton5ActionPerformed

    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {

            @Override
            public void run() {
                new JFUCFase3().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JButton jButton5;
    private javax.swing.JLabel jLRCu;
    private javax.swing.JLabel jLREu;
    private javax.swing.JLabel jLRNp;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JMenuItem jMLeFa;
    private javax.swing.JMenuItem jMTarifa;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem3;
    private javax.swing.JPanel jPChTar;
    private javax.swing.JPanel jPEntrada;
    private javax.swing.JPanel jPLeFa;
    private javax.swing.JPanel jPSaved;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JTextArea jTAEu;
    private javax.swing.JTextField jTCu;
    private javax.swing.JTextField jTLeAn;
    private javax.swing.JTextField jTLeAtu;
    private javax.swing.JTextField jTNp;
    private javax.swing.JTextField jTTarA;
    // End of variables declaration//GEN-END:variables
}
