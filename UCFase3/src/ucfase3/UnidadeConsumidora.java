package ucfase3;
public class UnidadeConsumidora {
    private int codigoUc;    
    private String nomeProprietario;
    private String enderecoUc;
    private int leituraAtual;
    private int leituraAnterior;
    private double valorTarifa;
    
    //Construtor
    public UnidadeConsumidora(int cu,String np,String eu,double vt){
        codigoUc=cu;
        nomeProprietario=np;
        enderecoUc=eu;
        valorTarifa=vt;
    }
    //Setters and getters

    public void setValorTarifa(double valorTarifa) {
        this.valorTarifa = valorTarifa;
    }

    public int getCodigoUc() {
        return codigoUc;
    }

    public String getEnderecoUc() {
        return enderecoUc;
    }

    public String getNomeProprietario() {
        return nomeProprietario;
    }

    public double getValorTarifa() {
        return valorTarifa;
    }
    
    public int getLeituraAnterior() {
        return leituraAnterior;
    }

    public void setLeituraAnterior(int leituraAnterior) {
        this.leituraAnterior = leituraAnterior;
    }

    public int getLeituraAtual() {
        return leituraAtual;
    }

    public void setLeituraAtual(int leituraAtual) {
        this.leituraAtual = leituraAtual;
    }
    //retorna o valor a pagar
    public Double ValorAPagar() {
        double aux;
        if (this.leituraAtual<this.leituraAnterior) {
            aux=((10000+this.leituraAtual)-this.leituraAnterior)*this.valorTarifa;
            return aux;
        } else {
            aux=(this.leituraAtual-this.leituraAnterior)*this.valorTarifa;
            return aux;
        }
    }
}
