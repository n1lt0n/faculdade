/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package parimpar;

/**
 *
 * @author n1lt0n
 */
public class ImparParPositivoNegativo {
    private int numero;

    public ImparParPositivoNegativo(int n) {
        this.numero = n;
    }

    public String resultado(){
        String res = "O número ";
        if(this.numero%2==0){
            res=res+"é Par e ";
        } else {
            res=res+"é Impar e ";
        }
        if (this.numero>0) {
            res=res+"Positivo";
        } else {
            if(this.numero<0){
                res=res+"Negativo";
            } else {
                res=res+"Nulo";
            }
        }
        return res;
    }

}
