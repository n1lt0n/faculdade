create table Moeda (
	idMoeda int not null AUTO_INCREMENT PRIMARY KEY,
	nomeMoeda varchar(10) not null,
	siglaMoeda char(4) not null
);

create table Unidade (
	idUnidade int not null AUTO_INCREMENT PRIMARY KEY,
	nomeMoeda varchar(10) not null,
	siglaMoeda char(5) not null
);

create table Produto (
	idProduto int not null AUTO_INCREMENT PRIMARY KEY,
	nomeProduto varchar(20) not null,
	marcaProduto varchar(20) not null,
	precoUnitarioProduto double not null,
	pesoProduto double not null,
	unidadePesoProduto int not null references Unidade(idUnidade),
	moedaPrecoUnitarioProduto int not null references Moeda(idMoeda)
);

create table Estoque (
	idEstoque int not null AUTO_INCREMENT PRIMARY KEY,
	produtoEstoque int not null references Produto(idProduto),
	quantidadeProduto int not null,
	dataCompraEstoque date not null
);

create table Receita (
	idReceita int not null AUTO_INCREMENT PRIMARY KEY,
	tituloReceita varchar(20) not null,
	descricaoReceita text not null,
	autoraReceita varchar(20)
);