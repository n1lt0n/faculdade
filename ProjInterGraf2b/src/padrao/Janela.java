package padrao; /* Exercicio feito em Sala - Prof. ALDO */

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.AdjustmentEvent;
import java.awt.event.AdjustmentListener;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JSplitPane;

public class Janela extends JFrame {

    private JSplitPane pFundo;
    private JScrollBar bVerde, bVermelho, bAzul;
    private JPanel pEsq, pDir;

    public Janela() {
        Eventos manEvt = new Eventos();
        pFundo = new JSplitPane();

        pEsq = new JPanel(new GridLayout(1, 3));
        bVerde = new JScrollBar(JScrollBar.VERTICAL, 0, 1, 0, 255);
        bVerde.addAdjustmentListener(manEvt);
        pEsq.add(bVerde);
        bVermelho = new JScrollBar(JScrollBar.VERTICAL, 0, 1, 0, 255);
        bVermelho.addAdjustmentListener(manEvt);
        pEsq.add(bVermelho);
        bAzul = new JScrollBar(JScrollBar.VERTICAL,0,1,0,255);
        bAzul.addAdjustmentListener(manEvt);
        pEsq.add(bAzul);
        pDir = new JPanel();
        pDir.setBackground(Color.red);

        pFundo.setDividerLocation(150);
        pFundo.setLeftComponent(pEsq);
        pFundo.setRightComponent(pDir);
        add(pFundo);
    }
    
    class Eventos implements AdjustmentListener {

        @Override
        public void adjustmentValueChanged(AdjustmentEvent e) {
            pDir.setBackground(
                new Color(
                      bVermelho.getValue(),
                      bVerde.getValue(),
                      bAzul.getValue()
                    )
            );
        }
        
    }

    public static void main(String[] args) {
        Janela app = new Janela();
        app.setDefaultCloseOperation(EXIT_ON_CLOSE);
        app.setSize(250, 150);
        app.setLocationRelativeTo(null);
        app.setTitle("ScrollBars");
        app.setVisible(true);
    }
}
