/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package exercicio2;

import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author n1lt0n
 */
public class Main extends JFrame {

    JTextField entrada;
    JButton conv;

    public Main() {
        setTitle("Convertendo");
        setSize(800, 600);
        //adiciona um listener para a janela
        addWindowListener(new WindowAdapter() {
            //trata o evento de fechamento da janela

            @Override //sobrescreve o evento de fechamento de janela padrão da linguagem
            public void windowClosing(WindowEvent e) {
                //função que executa o fechamento da janela
                actionExit();
            }
        });

        JPanel convPanel = new JPanel();
        GridBagConstraints constraints;
        GridBagLayout layout = new GridBagLayout();
        convPanel.setLayout(layout);

        JLabel jLEntrada = new JLabel("Entrada:");
        constraints = new GridBagConstraints();
        constraints.anchor = GridBagConstraints.EAST;
        constraints.insets = new Insets(5, 5, 0, 0);
        layout.setConstraints(jLEntrada, constraints);
        convPanel.add(jLEntrada);

        entrada = new JTextField();
        constraints = new GridBagConstraints();
        constraints.fill = GridBagConstraints.HORIZONTAL;
        constraints.gridwidth = GridBagConstraints.REMAINDER;
        constraints.insets = new Insets(5, 5, 0, 5);
        layout.setConstraints(entrada, constraints);
        convPanel.add(entrada);

        conv=new JButton("Converter");
        conv.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                actionConv();
            }
        });
        constraints = new GridBagConstraints();
        constraints.gridwidth = GridBagConstraints.REMAINDER;
        constraints.insets = new Insets(5, 5, 5, 5);
        layout.setConstraints(conv, constraints);
        convPanel.add(conv);

        getContentPane().setLayout(new BorderLayout());
        getContentPane().add(convPanel, BorderLayout.NORTH);
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Main converter = new Main();
        converter.setVisible(true);
    }

    private void actionExit() {
        System.exit(0);
    }

    /*private void actionConv() {
        Integer resInt = Integer.parseInt(entrada.getText());
        Float resFloat = Float.parseFloat(entrada.getText());
        Byte resByte = Byte.parseByte(entrada.getText());
        Double resDouble = Double.parseDouble(entrada.getText());
        JOptionPane.showMessageDialog(Main.this, "Inteiro: " + resInt + "\nFloat: "+resFloat+"\nByte: "+resByte+"\nDouble: "+resDouble, "Conversão", JOptionPane.OK_OPTION);
    }*/
    private void actionConv(){
        Integer resInt = Integer.parseInt(entrada.getText());
        JOptionPane.showMessageDialog(Main.this, "Inteiro: "+resInt,"Conversão",JOptionPane.INFORMATION_MESSAGE);
    }
}
