/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package mediaaritmetica;


/**
 *
 * @author fcg.1155376925
 */
public class Media {
    private float nota1;
    private float nota2;
    private float mediaAprovacao;

    Media(float n1, float n2, float ma) {
        this.nota1 = n1;
        this.nota2 = n2;
        this.mediaAprovacao = ma;
    }

    public float calcularMedia(){
        return (nota1+nota2)/2;
    }

    public String obterMensagem(){
        float res = calcularMedia();
        if(res<mediaAprovacao){
            return "Média Obtida:"+res+" Média para Aprovação: "+ mediaAprovacao +" Resultado: REPROVADO";
        }else {
            return "Média Obtida:"+res+" Média para Aprovação: "+ mediaAprovacao +" Resultado: APROVADO";
        }
    }

}
