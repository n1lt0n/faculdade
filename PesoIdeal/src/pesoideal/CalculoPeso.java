/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pesoideal;

/**
 *
 * @author n1lt0n
 */
public class CalculoPeso {
    private double altura;
    private String sexo;

    public CalculoPeso(double a, String s) {
        this.altura=a;
        this.sexo=s;
    }

    public String resultado(){
        double aux;
        if(this.sexo == null ? "Masculino" == null : this.sexo.equals("Masculino")) {
            aux=(72.7*this.altura)-58;
            return "Seu peso Ideal é: "+aux;
        } else {
            aux=(62.1*this.altura)-44.7;
            return "Seu peso Ideal é: "+aux;
        }
    }
}
