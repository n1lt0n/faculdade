#include <iostream>

using namespace std;

#include "node0601.h"

Node::Node(char c):myChar(c), nextNode(0) {

}

Node::~Node() {
	if (nextNode) {
		delete nextNode;
	}
}

void Node::Display() const {
	cout << myChar;
	if (nextNode) {
		nextNode->Display();
	}
}

int Node::HowMany(char theChar) const {
	int myCount = 0;
	if (myChar == theChar) {
		myCount++;
	}
	if (nextNode) {
		return myCount + nextNode->HowMany(theChar);
	} else {
		return myCount;
	}
}

void Node::Insert(char theChar) {
	if (!nextNode) {
		nextNode = new Node(theChar);
	}else {
		nextNode->Insert(theChar);
	}
}
