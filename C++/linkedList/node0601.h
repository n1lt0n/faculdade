//arquivo header com a classe node
class Node {
	public:
		Node(char c);
		~Node();
		void Display() const;
		int HowMany (char c) const;
		void Insert(char c);

	private:
		char getChar();
		Node *GetNext();
		char myChar;
		Node *nextNode;
};
